output "environnement_name" {
    value = module.vpc.name
}

output "vpc_id" {
    value = module.vpc.vpc_id
}

output "public_subnets" {
    value = module.vpc.public_subnets
}

output "private_subnets" {
    value = module.vpc.private_subnets
}

output "private_ip" {
    value = module.vpc.private_subnets
}

output "securitu_group" {
    value = module.security_group.security_group_id
}