terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.24.0"
    }
  }
}


provider "aws" {
  region = "eu-west-3"
  profile = "terraform"
}


