data "terraform_remote_state" "networking" {
    backend = "s3"

    config = {
        bucket = "ddwwbucket21" 
        key = "state/dev-infrastructure"
        region = "eu-west-3"
        profile = "terraform"
    }
}

