module "ec2_cluster" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  version                = "~> 2.0"

  name                   = "my-cluster-slpitted"
  instance_count         = 1

  ami                    = "ami-03cf0248cabc9d98d"
  instance_type          = "t2.micro"
  key_name               = "worker_ec2"
  monitoring             = true
  vpc_security_group_ids = [data.terraform_remote_state.networking.outputs.securitu_group]
  subnet_id              = element(data.terraform_remote_state.networking.outputs.public_subnets, 0)

  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
  
}
