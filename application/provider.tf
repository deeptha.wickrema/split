terraform {
   backend "s3" {
    region  = "eu-west-3"
    encrypt = false
    profile = "terraform"

   }
}

provider "aws" {
  region = "eu-west-3"
  profile = "terraform"
}



